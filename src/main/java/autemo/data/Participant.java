package autemo.data;

import java.util.List;
import java.util.Objects;

/**
 */
public final class Participant {
    private final List<Qualificateur> qualificateur;
    private final String role;

    /**
     * @param qualificateur list of words to qualify a participant
     * @param role the role of the participant
     */
    public Participant(List<Qualificateur> qualificateur, String role) {
        this.qualificateur = qualificateur;
        this.role = role;
    }

    public List<Qualificateur> qualificateur() {
        return qualificateur;
    }

    public String role() {
        return role;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Participant) obj;
        return Objects.equals(this.qualificateur, that.qualificateur) &&
                Objects.equals(this.role, that.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(qualificateur, role);
    }

    @Override
    public String toString() {
        return "Participant[" +
                "qualificateur=" + qualificateur + ", " +
                "role=" + role + ']';
    }

}
