package autemo.data;

import java.util.Objects;

/**
 */
public final class Phrase {
    private final String txt;
    private final String emotion;
    private final Raison raison;

    /**
     * @param txt sentence analysed
     * @param emotion emotion associated to the sentence analysed
     * @param raison reason of the emotion
     */
    public Phrase(String txt, String emotion, Raison raison) {
        this.txt = txt;
        this.emotion = emotion;
        this.raison = raison;
    }

    public String txt() {
        return txt;
    }

    public String emotion() {
        return emotion;
    }

    public Raison raison() {
        return raison;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Phrase) obj;
        return Objects.equals(this.txt, that.txt) &&
                Objects.equals(this.emotion, that.emotion) &&
                Objects.equals(this.raison, that.raison);
    }

    @Override
    public int hashCode() {
        return Objects.hash(txt, emotion, raison);
    }

    @Override
    public String toString() {
        return "Phrase[" +
                "txt=" + txt + ", " +
                "emotion=" + emotion + ", " +
                "raison=" + raison + ']';
    }

}
