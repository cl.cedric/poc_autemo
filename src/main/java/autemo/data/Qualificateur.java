package autemo.data;

import java.util.Objects;

/**
 */
public final class Qualificateur {
    private final String qualif;
    private final int debut;
    private final int fin;

    /**
     * @param qualif word to qualify a participant
     * @param debut start index
     * @param fin end index
     */
    public Qualificateur(String qualif, int debut, int fin) {
        this.qualif = qualif;
        this.debut = debut;
        this.fin = fin;
    }

    public String qualif() {
        return qualif;
    }

    public int debut() {
        return debut;
    }

    public int fin() {
        return fin;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Qualificateur) obj;
        return Objects.equals(this.qualif, that.qualif) &&
                this.debut == that.debut &&
                this.fin == that.fin;
    }

    @Override
    public int hashCode() {
        return Objects.hash(qualif, debut, fin);
    }

    @Override
    public String toString() {
        return "Qualificateur[" +
                "qualif=" + qualif + ", " +
                "debut=" + debut + ", " +
                "fin=" + fin + ']';
    }

}
