package autemo.data;

import org.json.simple.JSONArray;

import java.util.List;
import java.util.Objects;

/**
 */
public final class FERA {
    private final String scene;
    private final int id;
    private final List<Participant> participants;
    private final List<Phrase> phrases;
    private final String error;

    /**
     * @param scene The complete scene text
     * @param id The id of the scene
     * @param participants The list of participants
     * @param phrases The list of phrases
     * @param error The error message if there is one (String format)
     */
    public FERA(String scene, int id, List<Participant> participants, List<Phrase> phrases, String error) {
        this.scene = scene;
        this.id = id;
        this.participants = participants;
        this.phrases = phrases;
        this.error = error;
    }

    public String scene() {
        return scene;
    }

    public int id() {
        return id;
    }

    public List<Participant> participants() {
        return participants;
    }

    public List<Phrase> phrases() {
        return phrases;
    }

    public String error() {
        return error;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (FERA) obj;
        return Objects.equals(this.scene, that.scene) &&
                this.id == that.id &&
                Objects.equals(this.participants, that.participants) &&
                Objects.equals(this.phrases, that.phrases) &&
                Objects.equals(this.error, that.error);
    }

    @Override
    public int hashCode() {
        return Objects.hash(scene, id, participants, phrases, error);
    }

    @Override
    public String toString() {
        return "FERA[" +
                "scene=" + scene + ", " +
                "id=" + id + ", " +
                "participants=" + participants + ", " +
                "phrases=" + phrases + ", " +
                "error=" + error + ']';
    }

}
