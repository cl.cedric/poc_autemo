package autemo.data;

import java.util.Objects;

/**
 */
public final class Raison {
    private final int debut;
    private final int fin;

    /**
     * @param debut start index of the reason
     * @param fin end index of the reason
     */
    public Raison(int debut, int fin) {
        this.debut = debut;
        this.fin = fin;
    }

    public int debut() {
        return debut;
    }

    public int fin() {
        return fin;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Raison) obj;
        return this.debut == that.debut &&
                this.fin == that.fin;
    }

    @Override
    public int hashCode() {
        return Objects.hash(debut, fin);
    }

    @Override
    public String toString() {
        return "Raison[" +
                "debut=" + debut + ", " +
                "fin=" + fin + ']';
    }

}
