package autemo.segmentation;

import com.google.gson.Gson;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;


/**
 * Class with a static method to get all the sentences of a scene
 */
public class CoreNLPSegmentation implements ISegmentation {

    /**
     * Return a list of the scene's sentences.
     *
     * @param file the filename of the scene
     * @return ArrayList of sentences
     * @throws IOException
     * @throws ParseException
     * @throws InterruptedException
     */
    public static ArrayList<String> execute(String file) throws IOException, ParseException, InterruptedException {
//        ProcessBuilder pb = new ProcessBuilder("python", "python_scripts/StanzaSegmentation.py", file)
//                .inheritIO();
//        var process = pb.start();
//        process.waitFor();

//        var path = Path.of("json/stanza_sentences.json");
//
//        JSONParser parser = new JSONParser();
//        JSONObject json = (JSONObject) parser.parse(Files.readString(path));
//        return json;
        Gson gson = new Gson();

        // create a reader
        Reader reader = Files.newBufferedReader(Paths.get("json/stanza_sentences.json"));

        ArrayList<String> al = gson.fromJson(reader, ArrayList.class);
        return al;

    }
}
