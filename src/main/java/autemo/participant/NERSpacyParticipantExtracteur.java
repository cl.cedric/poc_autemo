package autemo.participant;

import autemo.data.Participant;
import autemo.data.Qualificateur;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Class with a static method to get the participants of a scene
 */
public class NERSpacyParticipantExtracteur implements IParticipantExtracteur {

    /**
     * Call a python script to get all the participants.
     * Return a list of participants. A participant is a qualifier and a role. A qualifier contains the qualifier,
     * a start index and an ending index
     * @return ArrayList of participants of the scene
     * @throws IOException
     * @throws InterruptedException
     * @throws ParseException
     */
    public static ArrayList<Participant> execute() throws IOException, InterruptedException, ParseException {
        if(!new File("json/stanza_entities.json").exists()) {
            ProcessBuilder pb = new ProcessBuilder("python", "python_scripts/TransformersEmotion.py")
                    .inheritIO();
            var process = pb.start();
            process.waitFor();
        }

        Gson gson = new Gson();

        Reader reader = Files.newBufferedReader(Paths.get("json/stanza_entities.json"));

        ArrayList<LinkedTreeMap<?, ?>> s = gson.fromJson(reader, ArrayList.class);
        ArrayList<Participant> ret = new ArrayList<>();
        s.forEach(e -> {
            ArrayList<Qualificateur> temp = new ArrayList<>();
            var qualif = new Qualificateur(
                    (String) e.get("text"),
                    ((Double) e.get("start_char")).intValue(),
                    ((Double) e.get("end_char")).intValue()
            );
            if((e.get("type")).equals("PER")) {
                temp.add(qualif);
            }
            if(!temp.isEmpty()) {
                ret.add(new Participant(
                        temp,
                        null
                ));
            }
        });
        return ret;
    }

}
