package autemo.emotion;

import autemo.data.Phrase;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Class with a static method to launch the emotion extraction from sentences in json
 */
public class ZeroShotCamemBERTEmotionExtracteur implements IEmotionExtracteur{

    /**
     * The method will call a python script to generate emotions if the json file is not found.
     * Then, the json file will be parsed to return the sentences associated with emotions
     *
     * @return ArrayList of Phrase objects with associated emotion
     * @throws IOException
     * @throws InterruptedException
     * @throws ParseException
     */
    public static ArrayList<Phrase> execute() throws IOException, InterruptedException, ParseException {
        ProcessBuilder pb = new ProcessBuilder("python", "python_scripts/TransformersEmotion.py")
                .inheritIO();
        var process = pb.start();
        process.waitFor();

        Gson gson = new Gson();

        Reader reader = Files.newBufferedReader(Paths.get("json/emotions.json"));

        ArrayList<LinkedTreeMap<?, ?>> s = gson.fromJson(reader, ArrayList.class);
        ArrayList<Phrase> ret = new ArrayList<>();
        s.forEach(e -> ret.add(new Phrase((String) e.get("sentence"), (String) e.get("emotion"), null)));
        return ret;
    }

}
