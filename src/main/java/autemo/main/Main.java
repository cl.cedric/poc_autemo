package autemo.main;

import autemo.data.FERA;
import autemo.data.Participant;
import autemo.data.Phrase;
import autemo.emotion.ZeroShotCamemBERTEmotionExtracteur;
import autemo.participant.NERSpacyParticipantExtracteur;
import autemo.segmentation.CoreNLPSegmentation;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Class to run the analysis
 */
/*
{
   "scène": string,
   "id":int,
   "participants":[
     { [key:string]: {
         "qualificateurs":[
           {
             [key:string]:{"name": String, "debut": int, "fin": int }
           }
         ],
         "roles":string
     }}
   ],
   "phrases":[
     { [key:string]:{
         "texte":string,
         "émotion":"colère/peur/tristesse/joie",
         "raisons":[
           {
             [key:string]:{ "debut": int, "fin": int }
           }
         ]
     }}
   ],
   "error":null
 }
 */
public class Main {
    /**
     * Main to run the 3 analysis :
     *      sentence segmentation
     *      emotion analysis
     *      participants analysis
     * A FERA object is created at the end
     *
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {

        //get all scene
        String scene_file = "data/scene_client2.txt";
        String scene = Files.readString(Paths.get(scene_file));

        try {
            var c = CoreNLPSegmentation.execute(scene_file);
        } catch (IOException e) {
            e.printStackTrace();
            throw new UncheckedIOException(e);
        } catch (ParseException | InterruptedException e) {
            e.printStackTrace();
            throw new Error(e);
        }
        System.out.println("Sentence segmentation done");
        ArrayList<Phrase> phrases = null;
        try {
            phrases = ZeroShotCamemBERTEmotionExtracteur.execute();

        } catch (IOException e) {
            e.printStackTrace();
            throw new UncheckedIOException(e);
        } catch (InterruptedException | ParseException e) {
            e.printStackTrace();
            throw new Error(e);
        }
        System.out.println("Emotion analyse done");
        ArrayList<Participant> participants = null;
        try {
            participants = NERSpacyParticipantExtracteur.execute();

        } catch (IOException e) {
            e.printStackTrace();
            throw new UncheckedIOException(e);
        } catch (InterruptedException | ParseException e) {
            e.printStackTrace();
            throw new Error(e);
        }
        System.out.println("Participants analyse done");

        FERA grille = new FERA(scene,0, participants, phrases, null);
        System.out.println(grille);

    }
}
