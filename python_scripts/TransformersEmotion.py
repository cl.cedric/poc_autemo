from transformers import pipeline
import json

THRESHOLD = 0.3

def get_sentences_from_json(json_name):
    f = open(json_name, encoding="utf-8")
    data = json.load(f)
    return data

def get_emotions_from_sentence(emotions, score):
    emo = ""
    for i in range(len(emotions)):
        if score[i] >= THRESHOLD:
             emo += emotions[i] + ","
    if emo == "":
        return "neutre"
    return emo[:-1]

if __name__ == '__main__':

    classifier_fr = pipeline("zero-shot-classification",
                             model="BaptisteDoyen/camembert-base-xnli")
    labels = ["colère", "peur", "tristesse", "joie"]

    txt = get_sentences_from_json("json/stanza_sentences.json")
    hypothesis_template = "L'émotion de ce texte est {}."

    emotions = list()

    for phrase in txt:
        res = classifier_fr(phrase, candidate_labels=labels, hypothesis_template=hypothesis_template)
        emotions.append({"sentence": res["sequence"], "emotion": get_emotions_from_sentence(res["labels"], res["scores"])})

    try:
        with open("json/emotions.json", "w", encoding="utf-8") as outfile:
            json.dump(emotions, outfile, indent=4)

    except Exception as e:
        print(e)


