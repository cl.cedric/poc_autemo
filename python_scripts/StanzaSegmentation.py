import stanza
import sys
import json

if __name__ == '__main__':

    if len(sys.argv) != 2:
        raise ValueError('Wrong number of arguments')

    stanza.download('fr')
    nlp = stanza.Pipeline('fr')

    # txt = open(str(sys.argv), "r", encoding="utf-8")
    txt = open(sys.argv[1], "r", encoding="utf-8")
    scene = ""
    for line in txt:
        scene += line
    doc = nlp(scene)

    sentences = list()
    entities = list()

    for sentence in doc.sentences:
        sentences.append(sentence.text)

    for entity in doc.entities:
        entities.append(entity.to_dict())

    try:
        with open("json/stanza_sentences.json", "w", encoding="utf-8") as outfile:
            json.dump(sentences, outfile, indent=4)

        with open("json/stanza_entities.json", "w", encoding="utf-8") as outfile:
            json.dump(entities, outfile, indent=4)
    except Exception as e:
        print(e)
